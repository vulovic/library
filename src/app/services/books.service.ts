import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class BooksService {

  constructor(private http: Http) { }

  getBooks() {
    return this.http.get('http://localhost:3000/books')
                    .map(res => res.json());
  }

  deleteBook(id){
    return this.http.delete('http://localhost:3000/books/'+id)
                    .map(res => res.json());
  }

  postBook(book){
    return this.http.post('http://localhost:3000/books', book)
                    .map(res => res.json());
  }

  updateBook(book){
    return this.http.put('http://localhost:3000/books/'+ book.id, book)
                    .map(res => res.json());
  }

}
