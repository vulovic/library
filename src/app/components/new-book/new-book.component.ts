import { Component, OnInit } from '@angular/core';
import { BooksModel } from '../../models/books.model';

@Component({
  selector: 'app-new-book',
  templateUrl: './new-book.component.html'
})
export class NewBookComponent implements OnInit {

  book = {
    name: '',
    author: ''
  }

  constructor(private booksModel:BooksModel) { }

  ngOnInit() {
  }

  addBook(){
    this.booksModel.addNewBook(this.book);
  }

}
