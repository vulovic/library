import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BooksModel} from '../../models/books.model';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html'
})
export class BookComponent implements OnInit {

  id;
  name;
  author;

  constructor(private route: ActivatedRoute, private booksModel: BooksModel) { }

  ngOnInit() {
    this.route.params.subscribe( ({id}) => {
      this.id = id;
    });
    this.booksModel.initializeBooksWithCallback(this.setBookVars);
  }

  setBookVars = () => {
    this.booksModel.getBookById(this.id, ({name, author}) => {
      this.name = name;
      this.author = author;
    });
  }

}
