import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { BooksModel } from '../../models/books.model';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html'
})
export class EditBookComponent implements OnInit {

  id;
  name;
  author;

  constructor(private route: ActivatedRoute, private booksModel:BooksModel) { }

  ngOnInit() {
    this.route.params.subscribe(({id}) => this.id = id)
    this.booksModel.initializeBooksWithCallback(this.setBookVars);
  }

  setBookVars = () => {
    this.booksModel.getBookById(this.id, ({name, author}) => {
      this.name = name;
      this.author = author;
    });
  }

  updateBook(){
    const {id, name, author} = this;
    this.booksModel.updateBook({id, name, author});
  }
}
