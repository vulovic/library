import { Component, OnInit } from '@angular/core';
import {BooksModel} from '../../models/books.model';
import { Router } from '@angular/router';
import { BookLinkComponent } from '../book-link/book-link.component';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html'
})
export class BooksComponent implements OnInit {

  search = '';

  constructor(public booksModel: BooksModel, private router: Router) { }

  ngOnInit() {

  }

  deleteBook(id){
    this.booksModel.deleteBookById(id);
  }

  goToEdit(id){
    this.router.navigate(['books',id,'edit']);
  }

}
