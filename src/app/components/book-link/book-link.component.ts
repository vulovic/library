import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-book-link',
  templateUrl: './book-link.component.html'
})
export class BookLinkComponent implements OnInit {

  @Input('bookName') name;
  @Input('bookId') id;

  constructor() { }

  ngOnInit() {
  }

}
