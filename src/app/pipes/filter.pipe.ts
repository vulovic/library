import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(values: any, searchString: any): any {
    return values.filter((value)=>{
      return value.name.toLowerCase().indexOf(searchString.toLowerCase())>-1;
    });
  }

}
