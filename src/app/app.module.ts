import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { BooksComponent } from './components/books/books.component';
import { BookComponent } from './components/book/book.component';
import { NewBookComponent } from './components/new-book/new-book.component';
import { EditBookComponent } from './components/edit-book/edit-book.component';
import {RouterModule, Routes} from '@angular/router';
import {BooksModel} from './models/books.model';
import { BooksService } from './services/books.service';
import {HttpModule} from '@angular/http';
import { BookLinkComponent } from './components/book-link/book-link.component';
import { FilterPipe } from './pipes/filter.pipe';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'books', component: BooksComponent},
  {path: 'books/new', component: NewBookComponent},
  {path: 'books/:id', component: BookComponent},
  {path: 'books/:id/edit', component: EditBookComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BooksComponent,
    BookComponent,
    NewBookComponent,
    EditBookComponent,
    BookLinkComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule,
    FormsModule
  ],
  providers: [BooksModel, BooksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
