import {Injectable} from '@angular/core';
import {BooksService} from '../services/books.service';
import {Router} from "@angular/router";
@Injectable()
export class BooksModel{

  books = [];

  constructor(private booksService: BooksService, private router:Router){
    this.initializeBooks();
  }

  initializeBooks(){
    this.booksService.getBooks().subscribe((books) => {this.books = books});
  }

  initializeBooksWithCallback(clbck){
    if(!this.books || this.books.length < 1) {
      this.booksService.getBooks().subscribe((books) => {
        this.books = books;
        clbck();
      });
    } else clbck();
  }

  getBookById(id, clbk){
    for(let i = 0; i < this.books.length; i++){
      if(this.books[i].id == id){
        clbk(this.books[i]);
      }
    }
  }

  deleteBookById(id){
    this.booksService.deleteBook(id).subscribe(books => {
      this.getIndexOfBookById(id, (i)=>{
        this.books.splice(i, 1);
      })
    });
  }

  getIndexOfBookById(id, clbk){
    for(let i = 0; i < this.books.length; i++){
      if(this.books[i].id == id){
        clbk(i);
      }
    }
  }

  addNewBook(book){
    this.booksService.postBook(book).subscribe((book) => {
      this.books.push(book);
      this.router.navigate(['/books']);
    });
  }

  updateBook(book){
    this.booksService.updateBook(book).subscribe((book)=>{
      this.initializeBooks();
    })
  }
}
